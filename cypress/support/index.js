// ***********************************************************
// This example support/index.js is processed and
// loaded automatically before your test files.
//
// This is a great place to put global configuration and
// behavior that modifies Cypress.
//
// You can change the location of this file or turn off
// automatically serving support files with the
// 'supportFile' configuration option.
//
// You can read more here:
// https://on.cypress.io/configuration
// ***********************************************************

// Import commands.js using ES2015 syntax:
import './commands'

// Alternatively you can use CommonJS syntax:
// require('./commands')

//this commands will be run before each test - it ensures that user is logged in

const email = 'mdc@test.com'
const password = 'mdc123'
beforeEach(function () {
    cy.visit("/index.php?controller=authentication&back=my-account")
    cy.get("#email").type(email)
    cy.get("#passwd").type(password).type("{enter}")
    cy.contains(" My account")
 })