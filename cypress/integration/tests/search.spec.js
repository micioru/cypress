const search = '#search_query_top'

describe('Search functionality', function(){
    it('Check if items are visible after search', function(){
        cy.visit('/index.php')
        cy.get(search)
        .type('Dress')
        .type('{enter}')
        cy.contains('7 results have been found.')
    })

    it('Check if there is message when no results find', function(){
        cy.visit('/index.php')
        cy.get(search)
        .type('Bikini')
        .type('{enter}')
        cy.contains('No results were found for your search')
    })
})