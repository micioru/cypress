describe('Newsletter subscribe functionality', function(){
    it('Check if user is able to subscribe to newsletter', function(){
        cy.visit('/index.php')
        cy.get('#newsletter-input')
        .type('mdc@test.com')
        .type('{enter}')
        cy.contains('Newsletter : This email address is already registered.')
    })
})