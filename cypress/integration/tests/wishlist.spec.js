describe('Wishlist functionality', function(){
    it('Check if user can add item to wishlist', function(){
        cy.visit('/index.php')
        cy.get('#homefeatured > :nth-child(2)').click()
        cy.get("#wishlist_button").click()
        cy.get('.fancybox-item').click()
        cy.get('.account > span').click()
        cy.get('.icon-heart').click()
        cy.contains('Qty')
    })
})