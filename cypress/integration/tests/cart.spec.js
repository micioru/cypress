// variables for tests
const msg = 'Product successfully added to your shopping cart'
const searchUrl = '/index.php?controller=search&orderby=position&orderway=desc&search_query=Dress&submit_search='
const firstItem = ':nth-child(1) > .product-container > .right-block'
const secondItem = ':nth-child(2) > .product-container > .right-block'
const viewCart = '[title="View my shopping cart"]'
const addCart = '.exclusive > span'
const continueButton = '.continue > span'
const oneProduct = 'Your shopping cart contains: 1 Product'
const twoProducts = 'Your shopping cart contains: 2 Products'
// end of variables

describe('Add to cart functionality', function(){

    beforeEach(function () {
        cy.visit(searchUrl)
     })
    
    it('Check if user is able to add item to cart', function(){
        cy.get(firstItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.get(viewCart).click()
        cy.contains(oneProduct)
        cy.contains(msg)
    })

    it('Check if user is able to add few items to cart', function(){
        cy.get(firstItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.visit(searchUrl)
        cy.get(secondItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.get(viewCart).click()
        cy.contains(twoProducts)
    })

    it('Check if user is able to add item with custom options to cart', function(){
        cy.get(firstItem).click()
        cy.get('#group_1').select('L')
        cy.get('#color_13').click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.get(viewCart).click()
        cy.contains('Color : Orange, Size : L')
    })

    it('Check if user is able to remove item to cart', function(){
        cy.get(firstItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.get(viewCart).click()
        cy.get('.icon-trash').click()
        cy.contains('Your shopping cart is empty.')
    })

    it('Check if user is able to remove one of few items in cart', function(){
        cy.get(firstItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.visit(searchUrl)
        cy.get(secondItem).click()
        cy.get(addCart).click()
        cy.contains(msg)
        cy.get(continueButton).click()
        cy.get(viewCart).click()
        cy.contains(twoProducts)
        cy.get('.icon-trash').first().click()
        cy.contains(oneProduct)
    })
})