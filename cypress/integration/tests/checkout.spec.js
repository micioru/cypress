const proceed = '.cart_navigation > .button > span'
const url = '/index.php?controller=search&orderby=position&orderway=desc&search_query=Dress&submit_search='

describe('Checkout functionality', function(){
    it('Add item to cart and proceed with checkout', function(){
        cy.visit(url)
        cy.get(':nth-child(1) > .product-container > .right-block').click()
        cy.get('.exclusive > span').click()
        cy.get('.button-container > .button-medium > span').click()
        cy.get(proceed).click()
        cy.get('#ordermsg > .form-control').type('some comment')
        cy.get(proceed).click()
        cy.get(proceed).click()
        cy.contains('You must agree to the terms of service before continuing.')
        cy.get('.fancybox-item').click()
        cy.get('#cgv').click()
        cy.get(proceed).click()
        cy.get('.bankwire').click()
        cy.get('#cart_navigation > .button > span').click()
        cy.contains('Your order on My Store is complete.')
    })
})

